function addItem(vacancies, employers, status) {
  const rating = document.querySelector('.rating__elements');

  function sendEmployerId(e) {
    sessionStorage.removeItem('employerId');
    sessionStorage.setItem('employerId', e.target.dataset.employer);
  }
  
  function sendVacancyId(e) {
    sessionStorage.removeItem('vacancyId');
    sessionStorage.setItem('vacancyId', e.target.dataset.vacancy);
  }

  vacancies.forEach(vacancy => {

    let statusClass = '';

    switch(vacancy.status) {
      case 'offer':
        statusClass = 'status_type_1';
        break;
      case 'doNot':
        statusClass = 'status_type_2';
        break;
      case 'candidate':
        statusClass = 'status_type_3';
        break;
      case 'decline':
        statusClass = 'status_type_4';
        break;
      default:
        statusClass = 'status_type_1';
    }

    const ratingItem = `<section class="rating-item">
      <div class="rating-item__headline">
        <span class="rating-item__title">${vacancy.name}</span>
        <span class="rating-item__salary">${vacancy.salary}</span>
        <span class="status status_add_mrb ${statusClass}">${status[vacancy.status]}</span>
        <i class="fas fa-angle-down rating-item__ic"></i>
      </div>
      <div class="rating-item__info">
        <div class="rating-item__row">
          <div class="rating-item__parametr">
            <i class="fas fa-building"></i> 
            <span class="rating-item__subtitle">${employers[vacancy.employer].name}</span>
          </div>
          <div class="rating-item__parametr">
            <i class="fas fa-business-time"></i>
            <span class="rating-item__subtitle">${vacancy.workTime}</span>
          </div>
          <div class="rating-item__parametr">
            <i class="fas fa-car"></i>
            <span class="rating-item__subtitle">${vacancy.roadTime}</span>
          </div>
        </div>
        <div class="rating-item__row rating-item__row_type_links">
          <a data-employer="${vacancy.employer}" href="employer" class="rating-item__link rating-item__link_type_employer">Работодатель</a>
          <a data-vacancy="${vacancy.vacancyId}" href="vacancy" class="rating-item__link rating-item__link_type_vacancy rating-item__link_add_mrl">Вакансия</a>
        </div>
      </div>
    </section>
    `;
    rating.innerHTML += ratingItem;
  });

  const employerButton = document.querySelectorAll('.rating-item__link_type_employer');
  const vacancyButton = document.querySelectorAll('.rating-item__link_type_vacancy');

  employerButton.forEach(button => {
    button.addEventListener('click', sendEmployerId)
  })

  vacancyButton.forEach(button => {
    button.addEventListener('click', sendVacancyId)
  })
}

function accordion() {
  //accordion rating
  const items = document.querySelectorAll('.rating-item');

  function showContent() {
    const icon = this.firstElementChild.lastElementChild;

    icon.classList.toggle('rating-item__ic_add_rotate');
      
    const content = this.lastElementChild;

    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  }

  items.forEach(item => {
    item.addEventListener('click', showContent);
  })

  //multiselect
  const checkboxes = document.querySelector('.rating__checkboxes');
  const selectBox = document.querySelector('.rating__select-box');
  let expanded = false;
   
  function showCheckboxes() {
    const btn = this.lastElementChild;

    if (!expanded) {
      checkboxes.style.visibility = 'visible';
      checkboxes.style.opacity = '1';
      btn.classList.add('rating__select-ic_type_reverse');
      expanded = true;
    } else {
      checkboxes.style.visibility = 'hidden';
      checkboxes.style.opacity = '0';
      btn.classList.remove('rating__select-ic_type_reverse');
      expanded = false;
    }
  }

  selectBox.addEventListener('click', showCheckboxes);
}


function init() {
  let vacancies = sessionStorage.getItem('vacancies');
  vacancies = JSON.parse(vacancies);
  let employers = sessionStorage.getItem('employers');
  employers = JSON.parse(employers);
  let status = sessionStorage.getItem('status');
  status = JSON.parse(status);

  addItem(vacancies, employers, status);
  accordion();
}

window.onload = function() {
  init();
}
