const button = document.querySelector('.button_type_sign-in');

button.addEventListener('click', fetchDB);

function fetchDB() {
fetch('/rest/db/vacancies')
.then(res => {
  if (res.status === 200) {
    return res.json();
  } else {
    throw(console.error('failed connetct to DB'));
  }
})
.then(data => {
  sessionStorage.setItem('pageId', '0');
  sessionStorage.setItem('vacancies', JSON.stringify(data.vacancies));
  sessionStorage.setItem('employers', JSON.stringify(data.employers));
  sessionStorage.setItem('status', JSON.stringify(data.status));
})
.catch(err => {
  console.error(err);
})
}