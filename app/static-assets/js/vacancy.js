const vacancyTitle = document.querySelector('.vacancy__title');
const vacancySalary = document.querySelector('.vacancy__salary');
const vacancyStatus = document.querySelector('.status_type_vacancy');
const vacancyEmployer = document.querySelector('.vacancy__subtitle');
const vacancyDesc = document.querySelector('.vacancy__info_type_desc');
const vacancyStudy = document.querySelector('.vacancy__info_type_study');
const vacancyInterest = document.querySelector('.vacancy__info_type_interest');
const vacancyRoadTime = document.querySelector('.vacancy__info_type_roadTime');
const vacancyWorkTime = document.querySelector('.vacancy__info_type_workTime');
const vacancyComment = document.querySelector('.vacancy__info_type_comment');

const vacancyId = sessionStorage.getItem('vacancyId');
let vacancyData = sessionStorage.getItem('vacancies');
vacancyData = JSON.parse(vacancyData);
let employerData = sessionStorage.getItem('employers');
employerData = JSON.parse(employerData);
let statusData = sessionStorage.getItem('status');
statusData = JSON.parse(statusData);

function renderVacancy() {
  vacancyTitle.textContent = vacancyData[vacancyId].name;
  vacancySalary.textContent = vacancyData[vacancyId].salary;
  vacancyStatus.textContent = statusData[vacancyData[vacancyId].status];
  //add color to status
  vacancyEmployer.textContent = employerData[vacancyData[vacancyId].employer].name;
  vacancyStudy.textContent = vacancyData[vacancyId].study;
  vacancyInterest.textContent = vacancyData[vacancyId].interest;
  vacancyRoadTime.textContent = vacancyData[vacancyId].roadTime;
  vacancyWorkTime.textContent = vacancyData[vacancyId].workTime;
  vacancyComment.textContent = vacancyData[vacancyId].comment;
}

renderVacancy();

