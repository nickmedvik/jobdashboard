const title = document.querySelector('.employer__title');
const info = document.querySelector('.employer__info');
const img = document.querySelector('.employer__img');

const employerId = sessionStorage.getItem('employerId');
const employerData = sessionStorage.getItem('employers');

function renderEmployer(data, id) {
  const newData = JSON.parse(data);
  title.textContent = newData[id].name;
  info.textContent = newData[id].description;
  img.setAttribute('src', `../../static-assets/img/${newData[id].img}`);
}

renderEmployer(employerData, employerId);


