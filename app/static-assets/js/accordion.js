window.onload = function() {
  //accordion rating
  const items = document.querySelectorAll('.rating-item');

  function showContent() {
    const icon = this.firstElementChild.lastElementChild;

    icon.classList.toggle('rating-item__ic_add_rotate');
      
    const content = this.lastElementChild;

    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  }

  items.forEach(item => {
    item.addEventListener('click', showContent);
  })

  //multiselect
  const checkboxes = document.querySelector('.rating__checkboxes');
  const selectBox = document.querySelector('.rating__select-box');
  let expanded = false;
   
  function showCheckboxes() {
    const btn = this.lastElementChild;

    if (!expanded) {
      checkboxes.style.visibility = 'visible';
      checkboxes.style.opacity = '1';
      btn.classList.add('rating__select-ic_type_reverse');
      expanded = true;
    } else {
      checkboxes.style.visibility = 'hidden';
      checkboxes.style.opacity = '0';
      btn.classList.remove('rating__select-ic_type_reverse');
      expanded = false;
    }
  }

  selectBox.addEventListener('click', showCheckboxes);
}