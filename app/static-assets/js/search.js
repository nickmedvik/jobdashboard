const searchInput = document.querySelector('.rating__search');


let vacanciesItems = sessionStorage.getItem('vacancies');
vacanciesItems = JSON.parse(vacanciesItems);
let employerItems = sessionStorage.getItem('employers');
employerItems = JSON.parse(employerItems);

searchInput.addEventListener('keyup', search);

function search(e) {
  const filter = e.target.value.trim().toUpperCase();
  const ratingItems = document.querySelectorAll('.rating-item');

  for (let i = 0; i < vacanciesItems.length; i++) {
    const element = ratingItems[i];
    const a = vacanciesItems[i].name.toUpperCase().indexOf(filter) > -1;
    const b = employerItems[vacanciesItems[i].employer].name.toUpperCase().indexOf(filter) > -1;

    if (a || b) {
      element.style.display = 'block';
    } else {
      element.style.display = 'none';
    }
  }
}



